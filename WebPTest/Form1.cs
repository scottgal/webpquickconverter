﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebPTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            LoadFiles();
        }

        private void LoadFiles()
        {
            var files = Directory.GetFiles(@"C:\Users\galloways2\Dropbox\Camera Uploads", "*.jpg");
            Parallel.ForEach(files, ConvertFile);
        }

        private void ConvertFile(string fileName)
        {
            var source = new Bitmap(fileName);
            var outName = Path.GetFileNameWithoutExtension(fileName);
            var data = source.LockBits(
                new Rectangle(0, 0, source.Width, source.Height),
                ImageLockMode.ReadOnly,
                PixelFormat.Format24bppRgb);
            IntPtr unmanagedData;
            var size = WebPEncodeBGR(data.Scan0, source.Width, source.Height, data.Stride, 25, out unmanagedData);
            var managedData = new byte[size];
            Marshal.Copy(unmanagedData, managedData, 0, size);
            File.WriteAllBytes(Path.Combine(@"E:\galloways2\Desktop\Webptest\", outName + ".webp"), managedData);

 
            WebPFree(unmanagedData);
        }

        [DllImport("libwebp.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int WebPEncodeBGR(IntPtr rgb, int width, int height, int stride, float quality_factor,
            out IntPtr output);

        [DllImport("libwebp.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int WebPFree(IntPtr p);

        /*
         * size_t WebPEncodeRGB(const uint8_t* rgb, int width, int height, int stride, float quality_factor, uint8_t** output);
size_t WebPEncodeBGR(const uint8_t* bgr, int width, int height, int stride, float quality_factor, uint8_t** output);
size_t WebPEncodeRGBA(const uint8_t* rgba, int width, int height, int stride, float quality_factor, uint8_t** output);
size_t WebPEncodeBGRA(const uint8_t* bgra, int width, int height, int stride, float quality_factor, uint8_t** output);
         */
    }
}
